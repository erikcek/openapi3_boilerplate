export const responseValidationError = result => {
  throw result.context.makeValidationError(
    'Response schema not match specification',
    result.errors.map(i => i.location)
  );
};
