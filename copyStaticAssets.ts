import * as shell from 'shelljs';

shell.cp('-R', 'api/openapi/', 'dist/api/openapi/');
shell.cp('-R', 'api/utils/utils.js', 'dist/api/utils/utils.js');
shell.cp('-R', 'config/', 'dist/config/');
shell.cp('-R', 'log/', 'dist/log/');