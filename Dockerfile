FROM alpine:3.7 as build

# isntall  nodejs
RUN apk add --no-cache nodejs
# Create app directory
WORKDIR /usr/src/app

# copy package.json and package.lock in workdir
# A wildcard is used to ensure both package.json AND package-lock.json are copied
COPY package*.json ./

# Install app dependencies
# If there is no change in package.json -> this layer will not be executed
RUN npm install --production

COPY . .
# run production script
RUN npm run build

EXPOSE 80

CMD [ "npm", "run", "production" ]
