import * as express from 'express';
import * as SwaggerUi from 'swagger-ui-express';
import * as config from 'config';
import * as morgan from 'morgan';
import * as YAML from 'yamljs';
import * as fs from 'fs';
import * as exegesisExpress from 'exegesis-express';
import * as path from 'path';
import * as http from 'http';
import { responseValidationError } from './api/errors/errors';

const serverConfig = config.get('server');
async function createServer() {
  const openApiSpec = YAML.load(`${__dirname}/api/openapi/openapi.yaml`);
  const options = {
    controllers: path.resolve(__dirname, './api/controllers'),
    controllersPattern: '**/*.@(ts|js)',
    allowMissingControllers: false,
    authenticators: {
      BasicAuth: () => {
        console.log('a');
      }
    },
    onResponseValidationError: responseValidationError
  };

  const exegesisMiddleware = await exegesisExpress.middleware(
    path.resolve(__dirname, './api/openapi/openapi.yaml'),
    options
  );

  const app = express();

  // logging
  if (process.env.NODE_ENV === 'dev') {
    app.use(morgan(serverConfig['morgan'].format));
  }
  if (process.env.NODE_ENV === 'production') {
    app.use(
      morgan(serverConfig['morgan'].format, {
        stream: fs.createWriteStream(`${__dirname}/log/access.log`, {
          flags: 'a'
        })
      })
    );
  }

  // If you have any body parsers, this should go before them.
  app.use(exegesisMiddleware);

  app.use(
    `/api-docs`,
    SwaggerUi.serve,
    SwaggerUi.setup(openApiSpec, false, {
      displayOperationId: true
    })
  );


  app.use((req, res, next) => {
    // res.status(500);
    const response = {
      status_code: 500,
      name: 'INTERNAL_SERVER_ERROR',
      description: 'Unknown error occured'
    };
    res.json(response);
  });

  const server = http.createServer(app);
  server.listen(serverConfig['port']);
}

createServer();
